var DOWNLOAD_ACTIVITIES_TIMEOUT = 3 * 1000;
var DOWNLOAD_ACTIVITY_TIMEOUT = 15 * 1000;

// Possible values are [ "tcx", "gpx", "kml" ]
var downloadType = "kml";



var ACTIVITIES_URL = "https://connect.garmin.com/modern/proxy/activitylist-service/activities/search/activities";

var DOWNLOAD_URL = "https://connect.garmin.com/modern/proxy/download-service/export/" + downloadType + "/activity/";



setTimeout(
    function() {
        console.log(ACTIVITIES_URL + window.location.search + "&_=" + Math.random());
        jQuery.getJSON(ACTIVITIES_URL + window.location.search + "&_=" + Math.random(), function(activities){
            //console.log(activities);
            downloadRecursively(activities, 0);
       });
   }
   , DOWNLOAD_ACTIVITIES_TIMEOUT
);

function downloadRecursively (activities, index) {
    console.log(index);
    if (index < activities.length) {
        downloadActivity(activities[index].activityId);
        setTimeout(function () {downloadRecursively(activities, index+1)}, DOWNLOAD_ACTIVITY_TIMEOUT);
    }
}

function downloadActivity (activityId) {
    //console.log(activity.activityId);
    downloadUrl = DOWNLOAD_URL + activityId;
    //console.log(downloadUrl);
    window.location.href = downloadUrl;
}