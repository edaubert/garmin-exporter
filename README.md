# garmin-exporter

The goal of this project is to provide a simple way to download a set of activities from the page inside garminconnect.


# How it works
To do so open the activities page and filter what you want to see/export.
Then in your browser open the dev console and copy/paste the script named `exporter.js`

You can tweak some parameters and especially the timeout options according to you requirements.
You can also define the type of files you want to export (GPX, TCX, KML).

**If your browser is configure to ask you where to download the file then it will ask you for each and every file**